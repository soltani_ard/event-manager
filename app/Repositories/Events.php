<?php


namespace App\Repositories;

use App\Event;

class Events {

    public $user_id = 0;
    public function setID($user_id){
        $this->user_id = $user_id;
    }
    public function get() {
        return Event::where('user_id',$this->user_id)->get();
        
    }
}